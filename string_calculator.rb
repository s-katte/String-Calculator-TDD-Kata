class StringCalculator
  def add(numbers)
    return 0 if numbers.empty?

    delimiter = /,|\n/ # Default delimiters

    if numbers.start_with?("//")
      parts = numbers.split("\n", 2)
      custom_delimiter_part = parts[0][2..-1]
      numbers = parts[1]
      if custom_delimiter_part.start_with?('[') && custom_delimiter_part.end_with?(']')
        custom_delimiter = custom_delimiter_part[1...-1]
      else
        custom_delimiter = custom_delimiter_part
      end

      delimiter = Regexp.new(Regexp.escape(custom_delimiter) + "|\n")
    end

    num_array = numbers.split(delimiter).map(&:to_i)
    negative_numbers = num_array.select { |num| num < 0 }

    unless negative_numbers.empty?
      raise "negative numbers not allowed: #{negative_numbers.join(', ')}"
    end

    num_array.reject { |num| num > 1000 }.sum
  end
end
