require 'rspec'
require_relative 'string_calculator'

RSpec.describe StringCalculator, '#add' do
  let(:calculator) { StringCalculator.new }

  it 'returns 0 for an empty string' do
    expect(calculator.add("")).to eq(0)
  end

  it 'returns the number itself for a single number string' do
    expect(calculator.add("1")).to eq(1)
  end

  it 'returns the sum of two numbers' do
    expect(calculator.add("1,5")).to eq(6)
  end

  it 'returns the sum of multiple numbers' do
    expect(calculator.add("1,2,3,4,5")).to eq(15)
  end

  it 'returns the sum of numbers with new lines and commas' do
    expect(calculator.add("1\n2,3")).to eq(6)
  end

  it 'returns the sum of numbers with a custom delimiter' do
    expect(calculator.add("//;\n1;2")).to eq(3)
  end

  it 'returns the sum of numbers with a custom delimiter and new lines' do
    expect(calculator.add("//;\n1;2\n3")).to eq(6)
  end

  it 'raises an exception for a single negative number' do
    expect { calculator.add("1,-2,3") }.to raise_error("negative numbers not allowed: -2")
  end

  it 'raises an exception for multiple negative numbers' do
    expect { calculator.add("-1,2,-3") }.to raise_error("negative numbers not allowed: -1, -3")
  end

  it 'ignores numbers larger than 1000' do
    expect(calculator.add("2,1001")).to eq(2)
  end

  it 'ignores multiple numbers larger than 1000' do
    expect(calculator.add("1000,1001,1002")).to eq(1000)
  end

  it 'returns the sum of numbers with a custom delimiter of any length' do
    expect(calculator.add("//[***]\n1***2***3")).to eq(6)
  end
end
